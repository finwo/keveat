#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "keveat.h"

int keveat_command_get = 1;
int keveat_command_put = 2;

// struct keveat_command {
//   void *next;
//   char *key;
//   void (*callback)(char*,char*,void*);
//   void *udata;
//   int   command;
//   int   age;
// };

// struct keveat_context {
//   struct keveat_command *queue;
//   char *last_key;
//   FILE *fd;
// };

void keveat_command_free( struct keveat_context *ctx, struct keveat_command *cmd ) {
  struct keveat_command *found = NULL;
  if (ctx->queue == cmd) {
    ctx->queue = cmd->next;
    free(cmd);
  } else {
    found = ctx->queue;
    while(found) {
      if (found->next == cmd) {
        found->next = cmd->next;
        free(cmd);
      }
      found = found->next;
    }
  }
}

void keveat_poll( struct keveat_context *ctx ) {
  struct keveat_command *pcmd = NULL;
  struct keveat_command *cmd  = NULL;

  // Rewind if we're at the end
  // Increments age of commands
  // Kills too-old commands
  if (feof(ctx->fd)) {
    rewind(ctx->fd);
    if (ctx->last_key) free(ctx->last_key);
    ctx->last_key = NULL;
    if (ctx->queue) {
      cmd = ctx->queue;
      while(cmd) {

        // Increment age
        cmd->age++;

        // Kill old command
        if (cmd->age >= 2) {
          if (cmd->callback) {
            (*cmd->callback)( cmd->key, NULL, cmd->udata );
          }
          keveat_command_free( ctx, cmd );
          cmd = pcmd ? pcmd->next : ctx->queue;
          continue;
        }

        // Move to the next cmd
        pcmd = cmd;
        cmd  = cmd->next;
      }
      cmd  = NULL;
      pcmd = NULL;
    }
  }

  // If we don't have a queue, we're done
  if (!ctx->queue) return;

  // TODO: Fetch next key from fd
  // TODO: Check queue for matching request
  // TODO: Call matching requests
}

// Initialize keveat
struct keveat_context * keveat_open( char *filename ) {
  struct keveat_context *ctx;

  // Open data file
  FILE *fd = fopen( filename, "r+" );
  if (fd) {
    rewind(fd);
  } else {
    fprintf(stderr, "Could not open '%s'\n", filename);
    return NULL;
  }

  // Build the context
  ctx     = calloc(1,sizeof(struct keveat_context));
  ctx->fd = fd;

  return ctx;
};

void keveat_get( struct keveat_context *ctx, char *key, void (*callback)(char*,char*,void*), void *udata ) {
  struct keveat_command *cmd = calloc(1, sizeof(struct keveat_command));
  cmd->callback = callback;
  cmd->command  = keveat_command_get;
  cmd->key      = key;
  cmd->next     = ctx->queue;
  cmd->udata    = udata;
  ctx->queue    = cmd;
}
