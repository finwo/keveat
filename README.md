# keveat

Distributed eventually-consistent multi-layered key-value storage

## Layers

Keveat is separated into multiple layers to keep each individual block simple.

### Storage

Single-machine

This layer is a simple key-value storage, working on a single file or drive & knowing nothing of data structure. This layer only handles bytes, record names & record versions.

### Small-scale redundancy

Multi-machine, single-rack

Transparent wrapper around a single storage node, works together with connected redundancy machines to ensure no data is lost when storage nodes fail.

Wrapper around a single storage node, allowing you to connect multiple distrubution + storage nodes together providing redundancy. Ensures each record is stored on N (configurable) nodes for when hard-drives fail & re-provides the storage API to clients (as-if it's a storage node).

Small-scale redundancy can be stacked to provide single-datacenter reduncancy.

HINT:
int main(int argc, char * argv[]){
struct stat buffer;
int status;
int fd;
fd = open("/tmp/mmapped.bin", O_RDWR);
status = fstat(fd, &buffer);
printf("number of data items in file: %d\n", buffer.st_size/sizeof(int));
close(fd);
return 0;
}
