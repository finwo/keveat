#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "keveat.h"

long long mstime() {
  long long      ms;
  struct timeval tv;
  gettimeofday(&tv, NULL);
  ms  = (long long)tv.tv_sec  * 1000;
  ms += (long long)tv.tv_usec / 1000;
  return ms;
}

int main( int argc, char *argv[] ) {
  int c, i;
  unsigned int  iPort  = 0;
  long long     ttime  = mstime();
  long          init   = 0;
  char         *aPort  = NULL;
  char         *aData  = NULL;;
  struct keveat_context *kctx = NULL;

  // Usable arguments
  struct option longopts[] = {
    { "data", required_argument, NULL, 'd' },
    { "port", required_argument, NULL, 'p' },
    NULL,
  };

  // Fetch the args
  while((c = getopt_long(argc, argv, ":p:d:i", longopts, NULL)) != -1) {
    switch(c) {
      case 'p':
        aPort = optarg;
        iPort = atoi(aPort);
        break;
      case 'd':
        aData = optarg;
        break;
      case 0: break; // getopt_long set a variable, just keep going
      default:
        fprintf(stderr, "%s: option '%c' is invalid: ignored\n", argv[0], optopt);
        break;
    }
  }

  // Open file if given
  if (aData) kctx = keveat_open(aData);

  // Poll everything & waiting
  for(;;) {
    if (kctx) keveat_poll(kctx);

    // Loop @ 100 Hz
    ttime += 10;
    usleep((ttime-mstime())*1000);
  }

  return iPort;
}
