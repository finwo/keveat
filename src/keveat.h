#ifndef __KEVEAT_H__
#define __KEVEAT_H__

#include <stdio.h>

#ifndef NULL
#define NULL ((void*)0)
#endif

struct keveat_command {
  void *next;
  char *key;
  void (*callback)(char*,char*,void*);
  void *udata;
  int   command;
  int   age;
};

struct keveat_context {
  struct keveat_command *queue;
  char *last_key;
  FILE *fd;
};

void keveat_poll( struct keveat_context *ctx );
struct keveat_context * keveat_open( char *filename );
void keveat_get( struct keveat_context *ctx, char *key, void (*callback)(char*,char*,void*), void *udata );

#endif // __KEVEAT_H__
